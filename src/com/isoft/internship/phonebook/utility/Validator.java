package com.isoft.internship.phonebook.utility;

public class Validator {
    public boolean isValidPhoneNumber(String phoneNumber){
        String pattern = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$";
        if(phoneNumber.matches(pattern))
            return true;
        return false;

    }
}
