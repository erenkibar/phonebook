package com.isoft.internship.phonebook.utility;

import com.isoft.internship.phonebook.model.Contact;
import com.isoft.internship.phonebook.model.PhoneBook;
import com.isoft.internship.phonebook.model.SearchResult;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {
    PhoneBook phoneBook = PhoneBook.getInstance();
    Validator validator = new Validator();

    public Menu(){}

    /**
     * Shows the contents of the main menu.
     */
    public void mainMenu(){
        System.out.println("\n1. Insert a contact.");
        System.out.println("2. Search");
        System.out.println("3. Remove");
        System.out.println("4. Show the phonebook");
        System.out.println("5. Exit");
        System.out.println("Choice:");
        handleMainMenuInput(readInput());
    }

    /**
     * Handles the addition of new contacts to the phonebook.
     */

    public void insertMenu() {
        System.out.println("Enter the name:");
        String name = readTextInput();
        System.out.println("Enter the phone number");
        String phoneNumber = readTextInput();
        while(!validator.isValidPhoneNumber(phoneNumber))
            phoneNumber = readTextInput();
        System.out.println("Enter the address");
        String address = readTextInput();
        Contact contact = new Contact(name, phoneNumber, address);
        phoneBook.addContact(contact);
        mainMenu();
    }

    /**
     * Shows the menu headers.
     */
    public void showMenu(){
        System.out.printf("\nID\t\tName\t\t PhoneNumber\t\t Address\n");
        PhoneBook phoneBook = PhoneBook.getInstance();
        phoneBook.show();
    }

    /**
     * Shows the contents of the search menu.
     */
    public void searchMenu(){
        System.out.println("1. Search by name.");
        System.out.println("2. Search by phone number.");
        System.out.println("3. Search by address.");
        System.out.println("4. Go back to previous menu.");
        System.out.println("Choice:");
        handleSearchMenuInput(readInput());

    }

    /**
     * Reads the next line from the user.
     * @return String
     */
    public String readTextInput(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    /**
     * Reads the next integer from the user.
     * @return choice
     */
    public Integer readInput() {
        int choice = 0;
        try {
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();
        } catch (InputMismatchException e){
            mainMenu();
        }
        return choice;
    }

    /**
     * Handles input for the main menu.
     * @param choice Choice input from user.
     */
    public void handleMainMenuInput(int choice){
        switch (choice) {
            case 1:
                insertMenu();
                break;
            case 2:
                searchMenu();
                break;
            case 3:
                showMenu();
                System.out.println("Enter the ID of the contact you want to remove:");
                int id = readInput();
                phoneBook.deleteById(id);
                showMenu();
                mainMenu();
                break;
            case 4:
                showMenu();
                mainMenu();
                break;
            case 5:
                System.exit(0);
            default:
                System.out.println("\nInvalid input!");
                mainMenu();

        }
    }

    /**
     * Handles input for the search menu.
     * @param choice Choice input from user.
     */
    public void handleSearchMenuInput(Integer choice){
        switch(choice) {
            case 1:
                System.out.println("Enter the name to search for:");
                SearchResult result = phoneBook.searchByName(readTextInput());
                checkResult(result);
                break;
            case 2:
                result = phoneBook.searchByPhoneNumber(readTextInput());
                checkResult(result);
                break;
            case 3:
                result = phoneBook.searchByAddress(readTextInput());
                checkResult(result);
                break;
            case 4:
                mainMenu();
                break;
            default:
                System.out.println("\nInvalid input!");
                mainMenu();
        }
    }

    /**
     * Checks if the search result is null.
     * @param result SearchResult to be checked.
     */
    public void checkResult(SearchResult result) {
        if(result.isNull())
            System.out.println("No entries were found.");
        else
            System.out.println("Found " + result.getSize() + " result(s)." );
            phoneBook.show(result.getContactList());
            mainMenu();
    }
}