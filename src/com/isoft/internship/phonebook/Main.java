package com.isoft.internship.phonebook;

import com.isoft.internship.phonebook.model.Contact;
import com.isoft.internship.phonebook.model.PhoneBook;
import com.isoft.internship.phonebook.utility.Menu;

public class Main {

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.mainMenu();
    }
}
