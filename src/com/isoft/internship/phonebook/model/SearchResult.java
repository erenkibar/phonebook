package com.isoft.internship.phonebook.model;

import java.util.ArrayList;
import java.util.List;

public class SearchResult {
    private List<Contact> contactList = new ArrayList<Contact>();
    private int size;

    public SearchResult(List<Contact> contactList, int size) {
        this.contactList = contactList;
        this.size = size;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public int getSize() {
        return size;
    }

    public boolean isNull() {
        if(contactList.isEmpty())
            return true;
        return false;
    }
}
