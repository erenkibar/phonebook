package com.isoft.internship.phonebook.model;

import java.util.*;

/**
 * @author Eren Kibar
 */

public class PhoneBook {
    private static PhoneBook phoneBook = new PhoneBook();
    private List<Contact> contactList = new ArrayList<>();



    public static PhoneBook getInstance(){
        return phoneBook;
    }

    /**
     *  Adds a Contact into the contactList.
      * @param contact A Contact object.
     */
    public void addContact(Contact contact){
        contactList.add(contact);
    }

    /**
     * Searches the list for a given name.
     * @param name Name of the contact.
     * @return SearchResult
     */
    public SearchResult searchByName(String name){
        Iterator<Contact> iterator = contactList.iterator();
        List<Contact> searchList = new ArrayList<>();
        while(iterator.hasNext()){
            Contact contact = iterator.next();
            if (contact.getName().toLowerCase().contains(name.toLowerCase()))
                searchList.add(contact);
        }
        SearchResult result = new SearchResult(searchList, searchList.size());
        return result;
    }

    /**
     * Searches the list for a given number.
     * @param phoneNumber Phone number to search for.
     */
    public SearchResult searchByPhoneNumber(String phoneNumber){
        Iterator<Contact> iterator = contactList.iterator();
        List<Contact> searchList = new ArrayList<>();
        while(iterator.hasNext()){
            Contact contact = iterator.next();
            if (contact.getPhoneNumber().contains(phoneNumber))
                searchList.add(contact);
        }
        SearchResult result = new SearchResult(searchList, searchList.size());
        return result;

    }

    /**
     * Searches the list for a given address.
     * @param address
     */
    public SearchResult searchByAddress(String address){
        Iterator<Contact> iterator = contactList.iterator();
        List<Contact> searchList = new ArrayList<>();
        while(iterator.hasNext()){
            Contact contact = iterator.next();
            if (contact.getAddress().toLowerCase().contains(address.toLowerCase()))
                searchList.add(contact);
        }
        SearchResult result = new SearchResult(searchList, searchList.size());
        return result;
    }

    /**
     * Deletes an entry from the phone book based on the given id.
     * @param id id of the contact.
     */
    public void deleteById(int id){
        contactList.removeIf(contact -> contact.getId() == id);
    }

    /**
     * Shows the contacts in the contact list.
     */
    public void show(){
        for (Contact contact : contactList)
            System.out.println(contact.show());
    }

    /**
     * Shows the contacts for a given Contact List.
     * @param searchList List of Contacts from a Search Result.
     */
    public void show(List<Contact> searchList){
        for (Contact contact : searchList)
            System.out.println(contact.show());

    }
}
